#include<stdio.h>
#include<math.h>
int main()
{
    int a,b,c,d,e;
    float r1,r2,rpart, ipart;
    printf("enter the coeffecients of the quadratic equation");
    scanf("%d%d%d", &a,&b,&c);
    d = (b*b-4*a*c);
    if(d>0)
        e=1;
    else if(d==0)
        e=2;
    else 
        e=3;
    switch(e)
    {
    case 1 : 
        printf("the roots are real and unequal");
        r1 = (-b+sqrt(d)/2*a);
        r2 = (-b-sqrt(d)/2*a);
        printf("the roots are %f and %f", r1, r2);
        break;
    case 2: 
        printf("the roots are real and equal");
        r1= (-b/2*a);
        printf("the root is %f", r1);
        break;
    case 3:
        printf("the roots are imaginary");
        d = sqrt(abs(d));
        rpart = -b/2*a;
        ipart = d/2*a;
        printf("the root is r1 = %.3f + i %.3f", rpart, ipart);
        printf("the root is r2 = %.3f - i %.3f", rpart, ipart);
        break;
    }
    return 0;
}