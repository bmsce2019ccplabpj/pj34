#include<stdio.h>
int input()
{
    int a;
    printf("enter a number");
    scanf("%d", &a);
    return a;
}
int rev(int a)
{
    int r=0;
    while(a!=0)
    {
        r=r*10;
        r=r+a%10;
        a=a/10;
    }
    return r;
}
int output(int r)
{
printf("the reversed number is %d",r);
}
int main()
{
    int a,b;
    a=input();
    b=rev(a);
    output(b);
    return 0;
}