#include<stdio.h>
#include<math.h>
int main()
{
    int a,b,c,d;
    float r1,r2;
    printf("enter the coeffecients of the quadratic equation");
    scanf("%d%d%d", &a,&b,&c);
    d = sqrt(b*b-4*a*c);
    if (d>0)
    {
        printf("the roots are real and unequal");
        r1 = -b+d/2*a;
        r2 = -b-d/2*a;
        printf("the roots are %f and %f",r1, r2);
    }
    else if (d==0)
    {
        printf("the roots are real and equal");
        r1 = -b/2*a;
        printf("the root is %f", r1);
    }
    else 
    {
        printf("the roots are imaginary");
    }
    return 0;
}