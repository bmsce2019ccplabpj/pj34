#include<stdio.h>
void size(int *r,int *c)
{
	printf("Enter the number of rows and columns:\n");
	scanf("%d%d",r,c);
}
void input(int r,int c,int marks[r][c])
{
	int i,j;
	for(i=0;i<r;i++)
	{
		printf("Enter the marks of student %d in 3 subjects:\n",i+1);
		for(j=0;j<c;j++)
		{
			scanf("%d",&marks[i][j]);
		}
	}
	return ;
}
void compute(int r,int c,int marks[r][c])
{
	int i,j,h;
	for(j=0;j<c;j++)
	{
		h=marks[0][j];
		for(i=1;i<r;i++)
		{
			if(marks[i][j]>h)
			{
				h=marks[i][j];
			}
		}
		printf("The highest marks in the subject %d is %d\n",j+1,h);
	}
	return ;
}
int main()
{
	int r,c;
	size(&r,&c);
	int marks[r][c];
	input(r,c,marks);
	compute(r,c,marks);
	return 0;
}
