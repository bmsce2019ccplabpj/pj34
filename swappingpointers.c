#include <stdio.h>
int swap(int *x, int *y)
{
    int z;
    z=*x;
    *x=*y;
    *y=z;
}
int main()
{
    int x,y;
    printf("enter the value of x:");
    scanf("%d",&x);
    printf("enter the value of y:");
    scanf("%d",&y);
    swap(&x,&y);
    printf("after swapping, the value of x = %d and value of y = %d", x,y);
    return 0;
}